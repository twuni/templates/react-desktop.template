import App from './app';
import { Renderer } from '@nodegui/react-nodegui';

const title = 'Demo';
process.title = title;

Renderer.render(<App title={title}/>);
