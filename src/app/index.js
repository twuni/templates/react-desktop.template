import { Text, View } from '@nodegui/react-nodegui';

import Layout from '../ui/layout';

const App = ({ title }) => <Layout title={title}>
  <View>
    <Text>{title}</Text>
  </View>
</Layout>;

export default App;
