import { Window } from '@nodegui/react-nodegui';

const Layout = ({ children, title }) => <Window
  minSize={{ height: 600, width: 800 }}
  windowTitle={title}
>{children}</Window>;

export default Layout;
