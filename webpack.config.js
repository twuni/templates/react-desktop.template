const path = require('path');

module.exports = (env, argv) => ({
  entry: [
    './src/index.js'
  ],
  mode: 'production',
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheCompression: false,
            cacheDirectory: true
          }
        }
      },
      {
        include: /node_modules/,
        test: /\.node$/i,
        use: [
          {
            loader: 'native-addon-loader',
            options: {
              name: '[name]-[hash].[ext]'
            }
          }
        ]
      }
    ]
  },
  node: {
    __dirname: true,
    __filename: true
  },
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'lib')
  },
  plugins: [
  ],
  resolve: {
    extensions: [
      '.js'
    ]
  },
  target: 'node'
});
